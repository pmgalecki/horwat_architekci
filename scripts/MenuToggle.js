// script for toggling menu
const button = document.querySelector('.header__toggle')
const menu = document.querySelector('.topbar__menu')

const menuToggle = () => {
    menu.classList.toggle('menu-open')
    menu.classList.toggle('menu-close')
}

button.addEventListener('click', menuToggle);

