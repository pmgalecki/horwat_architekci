'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');

//style paths
const sassFiles = 'styles/sass/**/*.scss';
const cssDest = 'styles/css/';

gulp.task('styles', () => {
    gulp.src(sassFiles)
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(gulp.dest(cssDest))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(browserSync.stream());
});

gulp.task('serve', ['styles'], () => {

    browserSync.init({
        server: './'
    });

    gulp.watch(sassFiles, ['styles']);
    gulp.watch('*.html').on('change', browserSync.reload);
});

gulp.task('default', ['serve']);